<?php
/**
 * @file
 * Primary hook implementations.
 */

/**
 * Implements hook_menu().
 */
function alt_login_page_menu() {
  $items = array();

  $items['admin/config/people/alt-login-page'] = array(
    'title' => 'Alt Login Page',
    'description' => 'Control the options for the alternative login page.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('alt_login_page_settings_form'),
    'access arguments' => array('administer users'),
    'file' => 'alt_login_page.admin.inc',
  );

  $path = variable_get('alt_login_page_path');
  if (!empty($path)) {
    $items[$path] = array(
      'title' => 'User account',
      'page callback' => 'drupal_get_form',
      'page arguments' => array('alt_login_page_login_form'),
      'access callback' => TRUE,
      'file' => 'alt_login_page.pages.inc',
      'weight' => -10,
    );
    $items[$path . '/login'] = array(
      'title' => 'Log in',
      'access callback' => 'user_is_anonymous',
      'type' => MENU_DEFAULT_LOCAL_TASK,
    );
    $items[$path . '/password'] = array(
      'title' => 'Request new password',
      'page callback' => 'drupal_get_form',
      'page arguments' => array('alt_login_page_pass_form'),
      'access callback' => TRUE,
      'type' => MENU_LOCAL_TASK,
      'file' => 'alt_login_page.pages.inc',
    );
  }

  return $items;
}

/**
 * Implements hook_permission().
 */
function alt_login_page_permission() {
  return array(
    'use alt login page' => array(
      'title' => t('Use alternative login page'),
      'description' => t('Roles without this permission will be redirected to the normal site login page.'),
    ),
  );
}

/**
 * Implements hook_user_logout().
 *
 * Optionally override the normal logout process. This will not trigger the
 * normal Drupal hooks.
 *
 * @see alt_login_page_module_implements_alter()
 */
function alt_login_page_user_logout($account) {
  if (user_access('use alt login page')) {
    if (variable_get('alt_login_page_logout', FALSE)) {
      session_destroy();
      drupal_goto(variable_get('alt_login_page_logout_path', '<front>'));
    }
  }
}

/**
 * Implements hook_module_implements_alter().
 *
 * Move this module's hook_user_logout to be run first.
 *
 * @see alt_login_page_user_logout()
 */
function alt_login_page_module_implements_alter(&$implementations, $hook) {
  if ($hook == 'user_logout') {
    $module = 'alt_login_page';
    $group = array($module => $implementations[$module]);
    unset($implementations[$module]);
    $implementations = $group + $implementations;
  }
}
